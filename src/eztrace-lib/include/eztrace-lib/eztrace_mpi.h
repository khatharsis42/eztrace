#ifndef EZTRACE_MPI_H
#define EZTRACE_MPI_H

enum EZT_MPI_Datatype {
  EZT_MPI_DATATYPE_NULL          ,
  EZT_MPI_CHAR                   ,
  EZT_MPI_UNSIGNED_CHAR          ,
  EZT_MPI_SHORT                  ,
  EZT_MPI_UNSIGNED_SHORT         ,
  EZT_MPI_INT                    ,
  EZT_MPI_UNSIGNED               ,
  EZT_MPI_LONG                   ,
  EZT_MPI_UNSIGNED_LONG          ,
  EZT_MPI_LONG_LONG_INT          ,
  EZT_MPI_LONG_LONG              ,
  EZT_MPI_FLOAT                  ,
  EZT_MPI_DOUBLE                 ,
  EZT_MPI_LONG_DOUBLE            ,
  EZT_MPI_BYTE                   ,
  EZT_MPI_WCHAR                  ,
  EZT_MPI_PACKED                 ,
  EZT_MPI_LB                     ,
  EZT_MPI_UB                     ,
  EZT_MPI_C_COMPLEX              ,
  EZT_MPI_C_FLOAT_COMPLEX        ,
  EZT_MPI_C_DOUBLE_COMPLEX       ,
  EZT_MPI_C_LONG_DOUBLE_COMPLEX  ,
  EZT_MPI_2INT                   ,
  EZT_MPI_C_BOOL                 ,
  EZT_MPI_SIGNED_CHAR            ,
  EZT_MPI_UNSIGNED_LONG_LONG     ,
  EZT_MPI_CHARACTER              ,
  EZT_MPI_INTEGER                ,
  EZT_MPI_REAL                   ,
  EZT_MPI_LOGICAL                ,
  EZT_MPI_COMPLEX                ,
  EZT_MPI_DOUBLE_PRECISION       ,
  EZT_MPI_2INTEGER               ,
  EZT_MPI_2REAL                  ,
  EZT_MPI_DOUBLE_COMPLEX         ,
  EZT_MPI_2DOUBLE_PRECISION      ,
  EZT_MPI_REAL4                  ,
  EZT_MPI_COMPLEX8               ,
  EZT_MPI_REAL8                  ,
  EZT_MPI_COMPLEX16              ,
  EZT_MPI_REAL16                 ,
  EZT_MPI_COMPLEX32              ,
  EZT_MPI_INTEGER1               ,
  EZT_MPI_INTEGER2               ,
  EZT_MPI_INTEGER4               ,
  EZT_MPI_INTEGER8               ,
  EZT_MPI_INTEGER16              ,
  EZT_MPI_INT8_T                 ,
  EZT_MPI_INT16_T                ,
  EZT_MPI_INT32_T                ,
  EZT_MPI_INT64_T                ,
  EZT_MPI_UINT8_T                ,
  EZT_MPI_UINT16_T               ,
  EZT_MPI_UINT32_T               ,
  EZT_MPI_UINT64_T               ,
  EZT_MPI_AINT                   ,
  EZT_MPI_OFFSET                 ,
  EZT_MPI_FLOAT_INT              ,
  EZT_MPI_DOUBLE_INT             ,
  EZT_MPI_LONG_INT               ,
  EZT_MPI_SHORT_INT              ,
  EZT_MPI_LONG_DOUBLE_INT        
};

enum EZT_MPI_Op {
    EZT_MPI_MAX,
    EZT_MPI_MIN,
    EZT_MPI_SUM,
    EZT_MPI_PROD,
    EZT_MPI_LAND,
    EZT_MPI_BAND,
    EZT_MPI_LOR,
    EZT_MPI_BOR,
    EZT_MPI_LXOR,
    EZT_MPI_BXOR,
    EZT_MPI_MAXLOC,
    EZT_MPI_MINLOC,
};

extern int (*EZT_MPI_Recv)(void* buffer, size_t size, int src, int tag);
extern int (*EZT_MPI_Send)(void* buffer, size_t size, int dest, int tag);
extern int (*EZT_MPI_Reduce)(const void *sendbuf, void *recvbuf, int count,
			     enum EZT_MPI_Datatype datatype,  enum EZT_MPI_Op op, int root);
extern int (*EZT_MPI_SetMPICollectiveCallbacks)(OTF2_Archive *archive);
extern int (*EZT_MPI_Barrier)();
extern double (*EZT_MPI_Wtime)();
#endif	/* EZTRACE_MPI_H */
