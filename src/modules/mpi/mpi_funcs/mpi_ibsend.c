/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Universite Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#ifndef _REENTRANT
#define _REENTRANT
#endif

#include "mpi_eztrace.h"

#include <dlfcn.h>
#include <eztrace-lib/eztrace.h>
#include <mpi.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <unistd.h>
#include <eztrace-core/eztrace_attributes.h>

static void MPI_Ibsend_prolog(CONST void* buf  MAYBE_UNUSED,
                              int count MAYBE_UNUSED,
                              MPI_Datatype datatype MAYBE_UNUSED,
                              int dest MAYBE_UNUSED,
                              int tag MAYBE_UNUSED,
                              MPI_Comm comm MAYBE_UNUSED,
                              MPI_Fint* req MAYBE_UNUSED) {
  if(!EZTRACE_SAFE) return;
  if(comm == MPI_COMM_NULL) return;

  int length;
  _EZT_MPI_Type_size(datatype, &length);
  length *= count;
  OTF2_ErrorCode err = OTF2_EvtWriter_MpiIsend(evt_writer,
					       NULL,
					       ezt_get_timestamp(),
					       dest,
					       MPI_TO_OTF_COMMUNICATOR(comm),
					       tag,
					       length,
					       (uint64_t)req);
  if(err != OTF2_SUCCESS) {
    eztrace_warn("OTF2 error: %s: %s\n", OTF2_Error_GetName(err), OTF2_Error_GetDescription(err));
  }
  ezt_mpi_set_request_type((MPI_Request*)req, send, comm, -1, -1, -1);
}

static int MPI_Ibsend_core(CONST void* buf,
			   int count,
			   MPI_Datatype datatype,
                           int dest,
			   int tag,
			   MPI_Comm comm,
			   MPI_Request* req) {
  return libMPI_Ibsend(buf, count, datatype, dest, tag, comm, req);
}


static void MPI_Ibsend_epilog(CONST void* buf  MAYBE_UNUSED,
                              int count MAYBE_UNUSED,
                              MPI_Datatype datatype MAYBE_UNUSED,
                              int dest MAYBE_UNUSED,
                              int tag MAYBE_UNUSED,
                              MPI_Comm comm MAYBE_UNUSED,
                              MPI_Fint* req MAYBE_UNUSED) {

}

int MPI_Ibsend(CONST void* buf,
	       int count,
	       MPI_Datatype datatype,
	       int dest,
               int tag,
	       MPI_Comm comm,
	       MPI_Request* req) {
  FUNCTION_ENTRY;
  MPI_Ibsend_prolog(buf, count, datatype, dest, tag, comm, (MPI_Fint*)req);
  int ret = MPI_Ibsend_core(buf, count, datatype, dest, tag, comm, req);
  MPI_Ibsend_epilog(buf, count, datatype, dest, tag, comm, (MPI_Fint*)req);
  FUNCTION_EXIT;
  return ret;
}

void mpif_ibsend_(void* buf,
		  int* count,
		  MPI_Fint* d,
		  int* dest,
		  int* tag,
                  MPI_Fint* c,
		  MPI_Fint* r,
		  int* error) {
  FUNCTION_ENTRY_("mpi_ibsend_");
  MPI_Comm c_comm = MPI_Comm_f2c(*c);
  MPI_Datatype c_type = MPI_Type_f2c(*d);
  MPI_Request c_req = MPI_Request_f2c(*r);
  MPI_Ibsend_prolog(buf, *count, c_type, *dest, *tag, c_comm, r);
  *error = MPI_Ibsend_core(buf, *count, c_type, *dest, *tag, c_comm, &c_req);
  *r = MPI_Request_c2f(c_req);
  MPI_Ibsend_epilog(buf, *count, c_type, *dest, *tag, c_comm, r);
  FUNCTION_EXIT_("mpi_ibsend_");
}
