#include "eztrace-core/eztrace_config.h"

#include <stdlib.h>

bool eztrace_autostart_enabled() {
  char* autostart_env = getenv("EZTRACE_AUTOSTART");
  if (autostart_env == NULL)
    return true;

  if (strcmp(autostart_env, "no") == 0 || strcmp(autostart_env, "No") || strcmp(autostart_env, "NO"))
    return false;

  return true;
}

