add_subdirectory(core)
add_subdirectory(instrumentation)
add_subdirectory(eztrace-lib)
add_subdirectory(modules)
add_subdirectory(plugin_creator)

#####################################################
# eztrace.preload

configure_file (
  ${CMAKE_CURRENT_SOURCE_DIR}/eztrace.preload.in
  ${CMAKE_CURRENT_BINARY_DIR}/eztrace.preload
  @ONLY # ask cmake to change only variables in the form @VAR@ (ie. don't touch the ${VAR} variables, which can be problematic for bash scripts)
  )

list(APPEND SCRIPTS
  ${CMAKE_CURRENT_BINARY_DIR}/eztrace.preload
  )

#####################################################
# eztrace

configure_file (${CMAKE_CURRENT_SOURCE_DIR}/eztrace.c.in ${CMAKE_CURRENT_BINARY_DIR}/eztrace.c)

add_executable(eztrace
  ${CMAKE_CURRENT_BINARY_DIR}/eztrace.c
)

target_compile_options(eztrace
  PRIVATE
    -Wall -Wextra
    -DEZTRACE_ABS_TOP_BUILDDIR="" # TODO
)

target_include_directories(eztrace
  PRIVATE
  ${CMAKE_SOURCE_DIR}/src/core/include/eztrace-core/
  )

target_link_libraries(eztrace
  PRIVATE
    ${OTF2_LIBRARY}
    eztrace-instrumentation
    eztrace-lib
)

#####################################################
# eztrace_avail
add_executable(eztrace_avail
  eztrace_avail.c
  )

target_compile_options(eztrace_avail
  PRIVATE
  -Wall -Wextra
  )

target_link_libraries(eztrace_avail
  PRIVATE
    ${OTF2_LIBRARY}
    eztrace-lib
)


################################################


#####################################################

install(PROGRAMS ${SCRIPTS} DESTINATION ${CMAKE_INSTALL_BINDIR})

install(
  TARGETS eztrace eztrace_avail
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
)
