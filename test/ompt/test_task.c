#include <omp.h>
#include <assert.h>
#include <stdio.h>
#include <stdint.h>


_Atomic int nb_tasks=0;

unsigned long long fib(int n) {
  unsigned long long x, y;
  if (n < 2) return n;
  #pragma omp task shared(x, n)
  nb_tasks += 1;
  x = fib(n-1);

  #pragma omp task shared(y, n)
  nb_tasks += 1;
  y = fib(n-2);

  #pragma omp taskwait
  return x+y;
}

int main() {
  int n = 10;
  unsigned long long res = 0;

  omp_set_num_threads(4);

#pragma omp parallel
  {
    res = fib(n);
  }
  printf("fibonnacci(%d) = %llu\n", n, res);
  assert(res == 55);
  printf("Number of executed tasks: %d\n", nb_tasks);
}
