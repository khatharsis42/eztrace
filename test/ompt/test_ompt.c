#include <omp.h>
#include <omp-tools.h>
#include <stdio.h>

int ompt_initialize(ompt_function_lookup_t lookup, int initial_device_num,
                    ompt_data_t *tool_data) {
  printf("[OMPT] Initialization done in %f\n",
         omp_get_wtime() - *(double *)(tool_data->ptr));
  *(double *)(tool_data->ptr) = omp_get_wtime();
  return 1; // success: activates tool
}

void ompt_finalize(ompt_data_t *tool_data) {
  printf("[OMPT] Terminaison. Application runtime: %f\n",
         omp_get_wtime() - *(double *)(tool_data->ptr));
}

ompt_start_tool_result_t *ompt_start_tool(unsigned int omp_version,
                                          const char *runtime_version) {
  printf("[OMPT] Initializing...\n");
  static double time = 0; // static defintion needs constant assigment
  time = omp_get_wtime();
  static ompt_start_tool_result_t ompt_start_tool_result = {
      &ompt_initialize, &ompt_finalize, {.ptr = &time}};
  return &ompt_start_tool_result; // success: registers tool
}

int main() {
#pragma omp parallel num_threads(4)
  {
    printf("Hello from thread %i of %i!\n", omp_get_thread_num(),
           omp_get_num_threads());
  }
  return 0;
}
