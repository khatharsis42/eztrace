/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 *
 *
 * alacon.h
 *
 *  Created on: 4 Aug. 2011
 *      Author: Charles Aulagnon <charles.aulagnon@inria.fr>
 */

#ifndef ALACON_H
#define ALACON_H

int foo(int, int);
int bar();

#endif
