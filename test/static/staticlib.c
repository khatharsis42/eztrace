/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 */
#define _GNU_SOURCE
#include <eztrace-lib/eztrace.h>
#include <eztrace-lib/eztrace_module.h>

#define CURRENT_MODULE staticlib
DECLARE_CURRENT_MODULE;

int (*libmafunc)(int n);

int mafunc(int n) {
  INTERCEPT_FUNCTION("mafunc", libmafunc);
  FUNCTION_ENTRY;
  int ret = libmafunc(n);
  FUNCTION_EXIT;
  return ret;
}

PPTRACE_START_INTERCEPT_FUNCTIONS(staticlib)
INTERCEPT3("mafunc", libmafunc)
PPTRACE_END_INTERCEPT_FUNCTIONS(staticlib)

static void init_staticlib() {
  INSTRUMENT_FUNCTIONS(staticlib);

  if (eztrace_autostart_enabled())
    eztrace_start();
}

static void finalize_staticlib() {
  eztrace_stop();
}

static void __staticlib_init(void) __attribute__((constructor));
static void __staticlib_init(void) {
  EZT_REGISTER_MODULE(staticlib, "Example module for static instrumentation",
		      init_staticlib, finalize_staticlib);
}
