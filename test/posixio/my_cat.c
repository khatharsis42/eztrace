#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>

int main(int argc, char**argv) {

  struct timeval t1, t2;
  gettimeofday(&t1, NULL);
  if(argc < 2) {
    fprintf(stderr, "Usage: %s inpu_file output_file\n", argv[0]);
    return EXIT_FAILURE;
  }
  FILE* f_in= fopen(argv[1], "r");
  FILE* f_out= fopen(argv[2], "w");
  assert(f_in);
  assert(f_out);
  sleep(1);
  int n_bytes=0;
  char c;
  while(fread(&c, sizeof(char), 1, f_in)) {
    fwrite(&c, sizeof(char), 1, f_out);
    n_bytes++;
  }
  fclose(f_in);
  fclose(f_out);

  gettimeofday(&t2, NULL);
  double duration = (t2.tv_sec - t1.tv_sec)*1e6 + (t2.tv_usec - t1.tv_usec);
  double rate=n_bytes/duration;
  printf("%d bytes copied in %lf usec -> %lf MB/s\n", n_bytes, duration, rate);

  int n_functions=2*n_bytes;
  double cost_per_function=duration/n_functions;
  printf("-> %lf usec/ function call\n", cost_per_function);
  return EXIT_SUCCESS;
}
