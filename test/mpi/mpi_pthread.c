#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <pthread.h>
#include <unistd.h>


void* thread_fun(void* arg)
{
	printf("Thread going to sleep\n");
	sleep(3);
	printf("Thread done sleeping\n");
}

int main(int argc, char* argv[])
{
	int rank, worldsize;

	int provided;
	MPI_Init_thread(&argc, &argv, MPI_THREAD_SERIALIZED, &provided);

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &worldsize);

	printf("Hello from rank %d ( / %d processes)\n", rank, worldsize);

	MPI_Barrier(MPI_COMM_WORLD);

	pthread_t thread;
	pthread_create(&thread, NULL, thread_fun, NULL);

	int v;

	if (rank == 0)
	{
		v = 23;
		MPI_Send(&v, 1, MPI_INT, 1, 0, MPI_COMM_WORLD);
	}
	else
	{
		MPI_Recv(&v, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		printf("[%d] received %d (expected %d)\n", rank, v, 23);
	}

	pthread_join(thread, NULL);

	MPI_Finalize();

	return EXIT_SUCCESS;
}
